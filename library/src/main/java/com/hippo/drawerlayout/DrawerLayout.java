/*
 * Copyright 2015 Hippo Seven
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hippo.drawerlayout;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

import androidx.annotation.ColorInt;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.MotionEventCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.customview.widget.ViewDragHelper;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SuppressLint("RtlHardcoded")
public class DrawerLayout extends ViewGroup implements ValueAnimator.AnimatorUpdateListener, Animator.AnimatorListener {

    @IntDef({STATE_CLOSED, STATE_SLIDING, STATE_OPEN})
    @Retention(RetentionPolicy.SOURCE)
    private @interface State {
    }

    private static final String KEY_SUPER = "super";
    private static final String KEY_OPENED_DRAWER = "opened_drawer";
    private static final String KEY_LEFT_LOCK_MODER = "left_lock_mode";
    private static final String KEY_RIGHT_LOCK_MODER = "right_lock_mode";

    private static final int[] THEME_ATTRS = {
            android.R.attr.colorPrimaryDark
    };

    private static final int[] LAYOUT_ATTRS = new int[]{
            android.R.attr.layout_gravity
    };

    private static final int DEFAULT_SCRIM_COLOR = 0x99000000;

    private static final int ANIMATE_TIME = 300;

    private static final int STATE_CLOSED = 0;
    private static final int STATE_SLIDING = 1;
    private static final int STATE_OPEN = 2;

    /**
     * The drawer is unlocked.
     */
    public static final int LOCK_MODE_UNLOCKED = 0;

    /**
     * The drawer is locked closed. The user may not open it, though the app may
     * open it programmatically.
     */
    public static final int LOCK_MODE_LOCKED_CLOSED = 1;

    /**
     * The drawer is locked open. The user may not close it, though the app may
     * close it programmatically.
     */
    public static final int LOCK_MODE_LOCKED_OPEN = 2;

    private static final float OPEN_SENSITIVITY = 0.1f;
    private static final float CLOSE_SENSITIVITY = 0.9f;

    private static final Interpolator DRAWER_INTERPOLATOR = new LinearOutSlowInInterpolator();

    private static final int MIN_DRAWER_MARGIN = 64;

    private int mMinDrawerMargin;
    private boolean mInLayout;

    private View mContentView;
    private View mLeftDrawer;
    private View mRightDrawer;

    private int mScrimColor = DEFAULT_SCRIM_COLOR;
    private float mScrimOpacity;
    private Paint mScrimPaint = new Paint();

    private ViewDragHelper mDragHelper;
    private float mInitialMotionX;
    private float mInitialMotionY;

    private boolean mLeftOpened;
    private boolean mRightOpened;
    @State
    private int mLeftState;
    @State
    private int mRightState;
    private float mLeftPercent;
    private float mRightPercent;
    private int mLeftLockMode;
    private int mRightLockMode;

    private ValueAnimator mAnimator;
    private View mTargetView;
    private int mStartLeft;
    private int mEndLeft;
    private boolean mToOpen;
    private boolean mCancelAnimation;
    private View mOpenTask;

    private DrawerListener mListener;

    private boolean mIntercepted;
    private boolean mCanIntercept;

    private final int mWindowPaddingTop = 0;
    private final int mWindowPaddingBottom = 0;

    private Drawable mStatusBarBackground;

    private CharSequence mTitleLeft;
    private CharSequence mTitleRight;

    private WindowInsetsCompat mLastInsets;
    private boolean mDrawStatusBarBackground;

    /**
     * Whether the drawer shadow comes from setting elevation on the drawer.
     */
    private static final boolean SET_DRAWER_SHADOW_FROM_ELEVATION =
            Build.VERSION.SDK_INT >= 21;

    private float mDrawerElevation;

    private GestureBlocker mGestureBlocker;

    /**
     * Listener for monitoring events about drawers.
     */
    public interface DrawerListener {
        /**
         * Called when a drawer's position changes.
         *
         * @param drawerView The child view that was moved
         * @param percent    The new offset of this drawer within its range, from 0-1
         */
        void onDrawerSlide(View drawerView, float percent);

        /**
         * Called when a drawer has settled in a completely open state.
         * The drawer is interactive at this point.
         *
         * @param drawerView Drawer view that is now open
         */
        void onDrawerOpened(View drawerView);

        /**
         * Called when a drawer has settled in a completely closed state.
         *
         * @param drawerView Drawer view that is now closed
         */
        void onDrawerClosed(View drawerView);

        /**
         * Called when the drawer motion state changes. The new state will
         * be one of {@link #STATE_CLOSED}, {@link #STATE_SLIDING} or {@link #STATE_OPEN}.
         *
         * @param newState The new drawer motion state
         */
        void onDrawerStateChanged(View drawerView, int newState);
    }

    public DrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public DrawerLayout(Context context, AttributeSet attrs,
                        int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        mLeftOpened = false;
        mRightOpened = false;
        mLeftState = STATE_CLOSED;
        mRightState = STATE_CLOSED;
        mLeftPercent = 0.0f;
        mRightPercent = 0.0f;
        mMinDrawerMargin = (int) (MIN_DRAWER_MARGIN * context.getResources().getDisplayMetrics().density + 0.5f);
        mDragHelper = ViewDragHelper.create(this, 0.5f, new DragHelperCallback());
        mAnimator = new ValueAnimator();
        mAnimator.setFloatValues(0.0f, 1.0f);
        mAnimator.addUpdateListener(this);
        mAnimator.addListener(this);
        mAnimator.setInterpolator(DRAWER_INTERPOLATOR);
        mCancelAnimation = false;

        setWillNotDraw(false);

        if (ViewCompat.getFitsSystemWindows(this)) {
            if (Build.VERSION.SDK_INT >= 21) {
                ViewCompat.setOnApplyWindowInsetsListener(this,
                        new androidx.core.view.OnApplyWindowInsetsListener() {
                            @Override
                            public WindowInsetsCompat onApplyWindowInsets(View view,
                                                                          WindowInsetsCompat insets) {
                                final DrawerLayout drawerLayout = (DrawerLayout) view;
                                drawerLayout.setChildInsets(insets,
                                        insets.getSystemWindowInsetTop() > 0);
                                return insets.consumeSystemWindowInsets();
                            }
                        });
                setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                final TypedArray a = context.obtainStyledAttributes(THEME_ATTRS);
                try {
                    mStatusBarBackground = a.getDrawable(0);
                } finally {
                    a.recycle();
                }
            } else {
                mStatusBarBackground = null;
            }
        }

        final TypedArray a = context
                .obtainStyledAttributes(attrs, R.styleable.DrawerLayout, defStyleAttr, 0);
        try {
            if (a.hasValue(R.styleable.DrawerLayout_elevation)) {
                mDrawerElevation = a.getDimension(R.styleable.DrawerLayout_elevation, 0);
            } else {
                mDrawerElevation = getResources().getDimension(R.dimen.def_drawer_elevation);
            }
        } finally {
            a.recycle();
        }
    }

    /**
     * Set a color to use for the scrim that obscures primary content while a drawer is open.
     *
     * @param color Color to use in 0xAARRGGBB format.
     */
    public void setScrimColor(@ColorInt int color) {
        mScrimColor = color;
        invalidate();
    }

    /**
     * Sets the base elevation of the drawer(s) relative to the parent, in pixels. Note that the
     * elevation change is only supported in API 21 and above.
     *
     * @param elevation The base depth position of the view, in pixels.
     */
    public void setDrawerElevation(float elevation) {
        mDrawerElevation = elevation;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            if (child == mLeftDrawer || child == mRightDrawer) {
                ViewCompat.setElevation(child, mDrawerElevation);
            }
        }
    }

    /**
     * The base elevation of the drawer(s) relative to the parent, in pixels. Note that the
     * elevation change is only supported in API 21 and above. For unsupported API levels, 0 will
     * be returned as the elevation.
     *
     * @return The base depth position of the view, in pixels.
     */
    public float getDrawerElevation() {
        if (SET_DRAWER_SHADOW_FROM_ELEVATION) {
            return mDrawerElevation;
        }
        return 0f;
    }

    /**
     * Set a drawable to draw in the insets area for the status bar.
     * Note that this will only be activated if this DrawerLayout fitsSystemWindows.
     *
     * @param bg Background drawable to draw behind the status bar
     */
    public void setStatusBarBackground(@Nullable Drawable bg) {
        mStatusBarBackground = bg;
        invalidate();
    }

    /**
     * Gets the drawable used to draw in the insets area for the status bar.
     *
     * @return The status bar background drawable, or null if none set
     */
    @Nullable
    public Drawable getStatusBarBackgroundDrawable() {
        return mStatusBarBackground;
    }

    /**
     * Set a drawable to draw in the insets area for the status bar.
     * Note that this will only be activated if this DrawerLayout fitsSystemWindows.
     *
     * @param resId Resource id of a background drawable to draw behind the status bar
     */
    public void setStatusBarBackground(int resId) {
        mStatusBarBackground = resId != 0 ? ContextCompat.getDrawable(getContext(), resId) : null;
        invalidate();
    }

    /**
     * Set a drawable to draw in the insets area for the status bar.
     * Note that this will only be activated if this DrawerLayout fitsSystemWindows.
     *
     * @param color Color to use as a background drawable to draw behind the status bar
     *              in 0xAARRGGBB format.
     */
    public void setStatusBarBackgroundColor(@ColorInt int color) {
        mStatusBarBackground = new ColorDrawable(color);
        invalidate();
    }

    public void setChildInsets(WindowInsetsCompat insets, boolean draw) {
        mLastInsets = insets;
        mDrawStatusBarBackground = draw;
        setWillNotDraw(!draw && getBackground() == null);
        requestLayout();
    }

    public void setDrawerListener(DrawerListener listener) {
        mListener = listener;
    }

    public void setGestureBlocker(GestureBlocker blocker) {
        mGestureBlocker = blocker;
    }

    /**
     * Close all currently open drawer views by animating them out of view.
     */
    public void closeDrawers() {
        closeDrawer(mLeftDrawer);
        closeDrawer(mRightDrawer);
    }

    /**
     * Open the specified drawer view by animating it into view.
     *
     * @param drawerView Drawer view to open
     */
    public void openDrawer(View drawerView) {
        if (drawerView == null || (drawerView != mLeftDrawer && drawerView != mRightDrawer))
            return;

        boolean isLeft = drawerView == mLeftDrawer;
        switch (isLeft ? mLeftState : mRightState) {
            case STATE_CLOSED:
                // Check other side first
                switch (isLeft ? mRightState : mLeftState) {
                    case STATE_CLOSED:
                        startAnimation(isLeft, true, false);
                        break;
                    case STATE_SLIDING:
                        if (mAnimator.isRunning()) {
                            if (mToOpen) {
                                cancelAnimation();
                                mOpenTask = isLeft ? mLeftDrawer : mRightDrawer;
                                startAnimation(!isLeft, false, false);
                            } else {
                                mOpenTask = isLeft ? mLeftDrawer : mRightDrawer;
                            }
                        } else {
                            // You finger is on Screen!
                        }
                        break;
                    case STATE_OPEN:
                        // Close other side first
                        mOpenTask = isLeft ? mLeftDrawer : mRightDrawer;
                        startAnimation(!isLeft, false, false);
                        break;
                }
                break;

            case STATE_SLIDING:
                if (mAnimator.isRunning()) {
                    if (mToOpen) {
                        // Same purpose
                    } else {
                        cancelAnimation();
                        startAnimation(isLeft, true, false);
                    }
                } else {
                    // You finger is on Screen!
                }
                break;

            case STATE_OPEN:
                // Ok it is opened
                break;
        }
    }

    public void openDrawer(int gravity) {
        if (gravity == Gravity.LEFT) {
            openDrawer(mLeftDrawer);
        } else if (gravity == Gravity.RIGHT) {
            openDrawer(mRightDrawer);
        }
    }

    /**
     * Close the specified drawer view by animating it into view.
     *
     * @param drawerView Drawer view to close
     */
    public void closeDrawer(View drawerView) {
        if (drawerView == null || (drawerView != mLeftDrawer && drawerView != mRightDrawer))
            return;

        boolean isLeft = drawerView == mLeftDrawer;
        switch (isLeft ? mLeftState : mRightState) {
            case STATE_CLOSED:
                // Ok it is closed
                break;
            case STATE_SLIDING:
                if (mAnimator.isRunning()) {
                    if (mToOpen) {
                        cancelAnimation();
                        startAnimation(isLeft, false, false);
                    } else {
                        // Same purpose
                    }
                } else {
                    // You finger is on Screen!
                }
                break;
            case STATE_OPEN:
                startAnimation(isLeft, false, false);
                break;
        }
    }

    public void closeDrawer(int gravity) {
        if (gravity == Gravity.LEFT) {
            closeDrawer(mLeftDrawer);
        } else if (gravity == Gravity.RIGHT) {
            closeDrawer(mRightDrawer);
        }
    }

    public boolean isDrawerOpen(View drawer) {
        if (drawer == mLeftDrawer) {
            return mLeftOpened;
        } else if (drawer == mRightDrawer) {
            return mRightOpened;
        } else {
            throw new IllegalArgumentException("The view is not drawer");
        }
    }

    public boolean isDrawerOpen(int gravity) {
        if (gravity == Gravity.LEFT) {
            return isDrawerOpen(mLeftDrawer);
        } else if (gravity == Gravity.RIGHT) {
            return isDrawerOpen(mRightDrawer);
        } else {
            throw new IllegalArgumentException("gravity must be Gravity.LEFT or Gravity.RIGHT");
        }
    }

    public void setDrawerLockMode(int lockMode, int gravity) {
        if (gravity == Gravity.LEFT) {
            setDrawerLockMode(lockMode, mLeftDrawer);
        } else if (gravity == Gravity.RIGHT) {
            setDrawerLockMode(lockMode, mRightDrawer);
        } else {
            throw new IllegalArgumentException("gravity must be Gravity.LEFT or Gravity.RIGHT");
        }
    }

    public void setDrawerLockMode(int lockMode, View drawer) {
        if (drawer != mLeftDrawer && drawer != mRightDrawer)
            throw new IllegalArgumentException("The view is not drawer");

        int oldLockMode;
        int otherSideLockMode;
        if (drawer == mLeftDrawer) {
            oldLockMode = mLeftLockMode;
            otherSideLockMode = mRightLockMode;
            mLeftLockMode = lockMode;
        } else {
            oldLockMode = mRightLockMode;
            otherSideLockMode = mLeftLockMode;
            mRightLockMode = lockMode;
        }
        if (oldLockMode == lockMode) {
            return;
        }

        if (otherSideLockMode == LOCK_MODE_LOCKED_OPEN && lockMode == LOCK_MODE_LOCKED_OPEN) {
            throw new IllegalArgumentException("Only on side could be LOCK_MODE_LOCKED_OPEN");
        }

        switch (lockMode) {
            // TODO What if open or close fail ?
            case LOCK_MODE_LOCKED_OPEN:
                openDrawer(drawer);
                break;
            case LOCK_MODE_LOCKED_CLOSED:
                closeDrawer(drawer);
                break;
        }
    }

    public int getDrawerLockMode(int gravity) {
        if (gravity == Gravity.LEFT) {
            return getDrawerLockMode(mLeftDrawer);
        } else if (gravity == Gravity.RIGHT) {
            return getDrawerLockMode(mRightDrawer);
        } else {
            throw new IllegalArgumentException("gravity must be Gravity.LEFT or Gravity.RIGHT");
        }
    }

    public int getDrawerLockMode(View drawer) {
        if (drawer == mLeftDrawer) {
            return mLeftLockMode;
        } else if (drawer == mRightDrawer) {
            return mRightLockMode;
        } else {
            throw new IllegalArgumentException("The view is not drawer");
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);
            final int absGravity = GravityCompat.getAbsoluteGravity(
                    ((LayoutParams) child.getLayoutParams()).gravity,
                    ViewCompat.getLayoutDirection(child));

            if (absGravity == Gravity.NO_GRAVITY) {
                if (mContentView != null)
                    throw new IllegalStateException("There is more than one content view");
                mContentView = child;
            } else if ((absGravity & Gravity.LEFT) == Gravity.LEFT) {
                if (mLeftDrawer != null)
                    throw new IllegalStateException("There is more than one left menu");
                mLeftDrawer = child;
            } else if ((absGravity & Gravity.RIGHT) == Gravity.RIGHT) {
                if (mRightDrawer != null)
                    throw new IllegalStateException("There is more than one right menu");
                mRightDrawer = child;
            } else {
                throw new IllegalStateException("Child " + child + " at index " + i +
                        " does not have a valid layout_gravity - must be Gravity.LEFT, " +
                        "Gravity.RIGHT or Gravity.NO_GRAVITY");
            }
        }

        if (mContentView == null) {
            throw new IllegalStateException("There is no content view");
        }
        // Material is solid.
        // Input events cannot pass through material.
        if (mLeftDrawer != null) {
            mLeftDrawer.setClickable(true);
        }
        if (mRightDrawer != null) {
            mRightDrawer.setClickable(true);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        if (widthMode != MeasureSpec.EXACTLY || heightMode != MeasureSpec.EXACTLY)
            throw new IllegalArgumentException(
                    "DrawerLayout must be measured with MeasureSpec.EXACTLY.");

        setMeasuredDimension(widthSize, heightSize);

        final boolean applyInsets = mLastInsets != null && ViewCompat.getFitsSystemWindows(this);

        // Gravity value for each drawer we've seen. Only one of each permitted.
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);

            if (child.getVisibility() == GONE) {
                continue;
            }

            final LayoutParams lp = (LayoutParams) child.getLayoutParams();

            if (applyInsets) {
                if (ViewCompat.getFitsSystemWindows(child)) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        WindowInsetsCompat wi = mLastInsets;
                        if (child == mLeftDrawer) {
                            wi = wi.replaceSystemWindowInsets(wi.getSystemWindowInsetLeft(),
                                    wi.getSystemWindowInsetTop(), 0,
                                    wi.getSystemWindowInsetBottom());
                        } else if (child == mRightDrawer) {
                            wi = wi.replaceSystemWindowInsets(0, wi.getSystemWindowInsetTop(),
                                    wi.getSystemWindowInsetRight(),
                                    wi.getSystemWindowInsetBottom());
                        }
                        ViewCompat.dispatchApplyWindowInsets(child, wi);
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= 21) {
                        WindowInsetsCompat wi = mLastInsets;
                        if (child == mLeftDrawer) {
                            wi = wi.replaceSystemWindowInsets(wi.getSystemWindowInsetLeft(),
                                    wi.getSystemWindowInsetTop(), 0,
                                    wi.getSystemWindowInsetBottom());
                        } else if (child == mRightDrawer) {
                            wi = wi.replaceSystemWindowInsets(0, wi.getSystemWindowInsetTop(),
                                    wi.getSystemWindowInsetRight(),
                                    wi.getSystemWindowInsetBottom());
                        }
                        lp.leftMargin = wi.getSystemWindowInsetLeft();
                        lp.topMargin = wi.getSystemWindowInsetTop();
                        lp.rightMargin = wi.getSystemWindowInsetRight();
                        lp.bottomMargin = wi.getSystemWindowInsetBottom();
                    }
                }
            }

            if (child == mContentView) {
                // Content views get measured at exactly the layout's size.
                final int contentWidthSpec = MeasureSpec.makeMeasureSpec(
                        widthSize - lp.leftMargin - lp.rightMargin, MeasureSpec.EXACTLY);
                final int contentHeightSpec = MeasureSpec.makeMeasureSpec(
                        heightSize - lp.topMargin - lp.bottomMargin, MeasureSpec.EXACTLY);
                child.measure(contentWidthSpec, contentHeightSpec);
            } else if (child == mLeftDrawer || child == mRightDrawer) {
                if (SET_DRAWER_SHADOW_FROM_ELEVATION) {
                    if (ViewCompat.getElevation(child) != mDrawerElevation) {
                        ViewCompat.setElevation(child, mDrawerElevation);
                    }
                }
                final int drawerWidthSpec = getChildMeasureSpec(widthMeasureSpec,
                        mMinDrawerMargin + lp.leftMargin + lp.rightMargin,
                        lp.width);
                final int drawerHeightSpec = getChildMeasureSpec(heightMeasureSpec,
                        lp.topMargin + lp.bottomMargin,
                        lp.height);
                child.measure(drawerWidthSpec, drawerHeightSpec);
            } else {
                throw new IllegalStateException("Don't call addView");
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        mInLayout = true;
        final int width = r - l;
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);

            if (child.getVisibility() == GONE) {
                continue;
            }

            final LayoutParams lp = (LayoutParams) child.getLayoutParams();
            if (child == mContentView) {
                child.layout(lp.leftMargin, lp.topMargin,
                        lp.leftMargin + child.getMeasuredWidth(),
                        lp.topMargin + child.getMeasuredHeight());
            } else { // Drawer, if it wasn't onMeasure would have thrown an exception.
                final int childWidth = child.getMeasuredWidth();
                final int childHeight = child.getMeasuredHeight();
                int childLeft;
                float percent;

                if (child == mLeftDrawer) {
                    percent = mLeftPercent;
                    childLeft = -childWidth + (int) (childWidth * percent);
                } else { // Right; onMeasure checked for us.
                    percent = mRightPercent;
                    childLeft = width - (int) (childWidth * percent);
                }

                final int vgrav = lp.gravity & Gravity.VERTICAL_GRAVITY_MASK;

                switch (vgrav) {
                    default:
                    case Gravity.TOP: {
                        child.layout(childLeft, lp.topMargin, childLeft + childWidth,
                                lp.topMargin + childHeight);
                        break;
                    }

                    case Gravity.BOTTOM: {
                        final int height = b - t;
                        child.layout(childLeft,
                                height - lp.bottomMargin - child.getMeasuredHeight(),
                                childLeft + childWidth,
                                height - lp.bottomMargin);
                        break;
                    }

                    case Gravity.CENTER_VERTICAL: {
                        final int height = b - t;
                        int childTop = (height - childHeight) / 2;

                        // Offset for margins. If things don't fit right because of
                        // bad measurement before, oh well.
                        if (childTop < lp.topMargin) {
                            childTop = lp.topMargin;
                        } else if (childTop + childHeight > height - lp.bottomMargin) {
                            childTop = height - lp.bottomMargin - childHeight;
                        }
                        child.layout(childLeft, childTop, childLeft + childWidth,
                                childTop + childHeight);
                        break;
                    }
                }

                final int newVisibility = percent > 0 ? VISIBLE : INVISIBLE;
                if (child.getVisibility() != newVisibility) {
                    child.setVisibility(newVisibility);
                }
            }
        }
        mInLayout = false;
    }

    @Override
    public void requestLayout() {
        if (!mInLayout) {
            super.requestLayout();
        }
    }

    private boolean isDrawerUnder(int x, int y) {
        return isViewUnder(mLeftDrawer, x, y) || isViewUnder(mRightDrawer, x, y);
    }

    private boolean isDrawersTouchable() {
        return (mLeftDrawer != null && mLeftLockMode == LOCK_MODE_UNLOCKED)
                || (mRightDrawer != null && mRightLockMode == LOCK_MODE_UNLOCKED);
    }

    private int getActualDxLeft(int dx) {
        int leftWidth = mLeftDrawer.getWidth();
        int oldLeft = mLeftDrawer.getLeft();
        int newLeft = clamp(oldLeft + dx, -leftWidth, 0);
        int newVisible = newLeft == -leftWidth ? View.INVISIBLE : View.VISIBLE;
        if (mLeftDrawer.getVisibility() != newVisible) {
            mLeftDrawer.setVisibility(newVisible);
        }

        updateDrawerSlide(mLeftDrawer, (newLeft + leftWidth) / (float) leftWidth);

        return newLeft - oldLeft;
    }

    private int getActualDxRight(int dx) {
        int width = getWidth();
        int rightWidth = mRightDrawer.getWidth();
        int oldLeft = mRightDrawer.getLeft();
        int newLeft = clamp(oldLeft + dx, width - rightWidth, width);
        int newVisible = newLeft == width ? View.INVISIBLE : View.VISIBLE;
        if (mRightDrawer.getVisibility() != newVisible) {
            mRightDrawer.setVisibility(newVisible);
        }

        updateDrawerSlide(mRightDrawer, (width - newLeft) / (float) rightWidth);

        return newLeft - oldLeft;
    }

    private void slideLeftDrawer(int dx) {
        mLeftDrawer.offsetLeftAndRight(getActualDxLeft(dx));
    }

    private void slideRightDrawer(int dx) {
        mRightDrawer.offsetLeftAndRight(getActualDxRight(dx));
    }

    @Override
    public void onAnimationUpdate(@NonNull ValueAnimator animation) {
        float value = (Float) animation.getAnimatedValue();
        int oldLeft = mTargetView.getLeft();
        int newLeft = lerp(mStartLeft, mEndLeft, value);
        if (mTargetView == mLeftDrawer) {
            slideLeftDrawer(newLeft - oldLeft);
        } else {
            slideRightDrawer(newLeft - oldLeft);
        }
    }

    @Override
    public void onAnimationStart(Animator animation) {
        updateDrawerState(mTargetView, STATE_SLIDING);
    }

    @Override
    public void onAnimationEnd(@NonNull Animator animation) {
        if (!mCancelAnimation) {
            updateDrawerSlide(mTargetView, mToOpen ? 1.0f : 0.0f);
            updateDrawerState(mTargetView, mToOpen ? STATE_OPEN : STATE_CLOSED);
            if (mToOpen) {
                dispatchOnDrawerOpened(mTargetView);
            } else {
                dispatchOnDrawerClosed(mTargetView);
            }

            if (mOpenTask != null && !mToOpen &&
                    (mOpenTask == mLeftDrawer || mOpenTask == mRightDrawer)) {
                // If call startAnimation directly,
                // onAnimationStart and onAnimationEnd will not be called
                final boolean isLeft = mOpenTask == mLeftDrawer;
                getHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        startAnimation(isLeft, true, false);
                    }
                });
            }
        }

        mCancelAnimation = false;
        mOpenTask = null;
    }

    @Override
    public void onAnimationCancel(Animator animation) {
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }

    private void startAnimation(final boolean isLeft, final boolean isOpen,
                                boolean staticDuration) {
        mToOpen = isOpen;
        int total;
        if (isLeft) {
            mTargetView = mLeftDrawer;
            mStartLeft = mLeftDrawer.getLeft();
            total = mLeftDrawer.getWidth();
            mEndLeft = mToOpen ? 0 : -total;
        } else {
            mTargetView = mRightDrawer;
            mStartLeft = mRightDrawer.getLeft();
            total = mRightDrawer.getWidth();
            mEndLeft = mToOpen ? getWidth() - total : getWidth();
        }

        if (mStartLeft == mEndLeft) {
            // No need to animate
            updateDrawerSlide(mTargetView, mToOpen ? 1.0f : 0.0f);
            updateDrawerState(mTargetView, mToOpen ? STATE_OPEN : STATE_CLOSED);
            if (mToOpen) {
                dispatchOnDrawerOpened(mTargetView);
            } else {
                dispatchOnDrawerClosed(mTargetView);
            }
            return;
        }

        mAnimator.setDuration(staticDuration ? ANIMATE_TIME : ANIMATE_TIME * Math.abs(mStartLeft - mEndLeft) / total);

        mAnimator.start();
    }

    private boolean shouldCloseDrawers(float x, float y) {
        View drawer = null;
        if (mLeftPercent > 0.0f) {
            drawer = mLeftDrawer;
        } else if (mRightPercent > 0.0f) {
            drawer = mRightDrawer;
        }
        if (drawer == null) {
            return false;
        }

        int xInt = (int) x;
        int yInt = (int) y;

        return !isViewUnder(drawer, xInt, yInt) &&
                yInt >= drawer.getTop() && yInt < drawer.getBottom();
    }

    /**
     * Returns {@code true} if any drawer is visible.
     */
    public boolean isDrawersVisible() {
        return mLeftPercent > 0 || mRightPercent > 0;
    }

    @Override
    public boolean onInterceptTouchEvent(@NonNull MotionEvent ev) {
        final int action = MotionEventCompat.getActionMasked(ev);
        final float x = ev.getX();
        final float y = ev.getY();

        if (!isDrawersTouchable()) {
            return false;
        }

        if (!shouldCloseDrawers(x, y) && mGestureBlocker != null && mGestureBlocker.shouldBlockGesture(ev)) {
            return false;
        }

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mIntercepted = shouldCloseDrawers(x, y);
                mCanIntercept = true;
                mInitialMotionX = x;
                mInitialMotionY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                final float adx = Math.abs(x - mInitialMotionX);
                final float ady = Math.abs(y - mInitialMotionY);
                final int slop = mDragHelper.getTouchSlop();
                if (ady > slop && ady > adx) {
                    mCanIntercept = false;
                }
                if (mCanIntercept && adx > slop && adx > ady) {
                    mIntercepted = true;
                }
                if (shouldCloseDrawers(x, y)) {
                    mIntercepted = true;
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                mDragHelper.cancel();
                if (shouldCloseDrawers(x, y)) {
                    closeDrawers();
                }
                break;
        }

        try {
            mDragHelper.shouldInterceptTouchEvent(ev);
        } catch (Throwable e) {
            // Ignore
        }

        return mIntercepted;
    }

    private void cancelAnimation() {
        if (mAnimator.isRunning()) {
            mOpenTask = null;
            mCancelAnimation = true;
            mAnimator.cancel();
        }
    }

    @Override
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent(@NonNull MotionEvent ev) {

        // Cancel animate
        cancelAnimation();

        try {
            mDragHelper.processTouchEvent(ev);
        } catch (Throwable e) {
            // Ignore
        }

        final int action = MotionEventCompat.getActionMasked(ev);
        final float x = ev.getX();
        final float y = ev.getY();

        if (action == MotionEvent.ACTION_DOWN) {
            if (!shouldCloseDrawers(x, y) && mGestureBlocker != null && mGestureBlocker.shouldBlockGesture(ev)) {
                return false;
            } else {
                mIntercepted = true;
            }
        }

        if (!mIntercepted) {
            return false;
        }

        if (!isDrawersTouchable()) {
            return false;
        }

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mInitialMotionX = x;
                mInitialMotionY = y;
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (mLeftState == STATE_SLIDING) {
                    if (mLeftOpened && mLeftPercent < CLOSE_SENSITIVITY)
                        startAnimation(true, false, false);
                    else if (!mLeftOpened && mLeftPercent < OPEN_SENSITIVITY)
                        startAnimation(true, false, true);
                    else if (mLeftOpened && mLeftPercent >= CLOSE_SENSITIVITY)
                        startAnimation(true, true, true);
                    else if (!mLeftOpened && mLeftPercent >= OPEN_SENSITIVITY)
                        startAnimation(true, true, false);
                } else if (mRightState == STATE_SLIDING) {
                    if (mRightOpened && mRightPercent < CLOSE_SENSITIVITY)
                        startAnimation(false, false, false);
                    else if (!mRightOpened && mRightPercent < OPEN_SENSITIVITY)
                        startAnimation(false, false, true);
                    else if (mRightOpened && mRightPercent >= CLOSE_SENSITIVITY)
                        startAnimation(false, true, true);
                    else if (!mRightOpened && mRightPercent >= OPEN_SENSITIVITY)
                        startAnimation(false, true, false);
                } else if (shouldCloseDrawers(x, y)) {
                    closeDrawers();
                }
                break;
        }

        return true;
    }

    @Override
    protected int getChildDrawingOrder(int childCount, int i) {
        return i;
    }

    private void updateDrawerSlide(View drawerView, float percent) {
        boolean update = false;
        // Update percent
        if (drawerView == mLeftDrawer) {
            update = mLeftPercent != percent;
            mLeftPercent = percent;
            mScrimOpacity = percent;
        } else if (drawerView == mRightDrawer) {
            update = mRightPercent != percent;
            mRightPercent = percent;
            mScrimOpacity = percent;
        }

        ViewCompat.postInvalidateOnAnimation(this);

        // Callback
        if (update && mListener != null) {
            mListener.onDrawerSlide(drawerView, percent);
        }
    }

    private void updateDrawerState(View drawerView, @State int state) {
        boolean update = false;
        // Update state
        if (drawerView == mLeftDrawer) {
            update = mLeftState != state;
            mLeftState = state;
        } else if (drawerView == mRightDrawer) {
            update = mRightState != state;
            mRightState = state;
        }

        // Callback
        if (update && mListener != null) {
            mListener.onDrawerStateChanged(drawerView, state);
        }
    }

    private void dispatchOnDrawerClosed(View drawerView) {
        boolean update = false;
        // Update
        if (drawerView == mLeftDrawer) {
            update = mLeftOpened;
            mLeftOpened = false;
        } else if (drawerView == mRightDrawer) {
            update = mRightOpened;
            mRightOpened = false;
        }

        // Callback
        if (update && mListener != null) {
            mListener.onDrawerClosed(drawerView);
        }
    }

    private void dispatchOnDrawerOpened(View drawerView) {
        boolean update = false;
        // Update
        if (drawerView == mLeftDrawer) {
            update = !mLeftOpened;
            mLeftOpened = true;
        } else if (drawerView == mRightDrawer) {
            update = !mRightOpened;
            mRightOpened = true;
        }

        // Callback
        if (update && mListener != null) {
            mListener.onDrawerOpened(drawerView);
        }
    }

    // Remove deprecation suppression once b/120984242 is resolved.
    @SuppressWarnings("deprecation")
    private static boolean hasOpaqueBackground(View v) {
        final Drawable bg = v.getBackground();
        if (bg != null) {
            return bg.getOpacity() == PixelFormat.OPAQUE;
        }
        return false;
    }

    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
        final int height = getHeight();
        final boolean drawingContent = child == mContentView;
        int clipLeft = 0, clipRight = getWidth();

        final int restoreCount = canvas.save();
        if (drawingContent) {
            final int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View v = getChildAt(i);
                if (v == child || v.getVisibility() != VISIBLE
                        || !hasOpaqueBackground(v) || v != mLeftDrawer || v != mRightDrawer
                        || v.getHeight() < height) {
                    continue;
                }

                if (v == mLeftDrawer) {
                    final int vright = v.getRight();
                    if (vright > clipLeft) clipLeft = vright;
                } else if (v == mRightDrawer) {
                    final int vleft = v.getLeft();
                    if (vleft < clipRight) clipRight = vleft;
                }
            }
            canvas.clipRect(clipLeft, 0, clipRight, getHeight());
        }
        final boolean result = super.drawChild(canvas, child, drawingTime);
        canvas.restoreToCount(restoreCount);

        if (mScrimOpacity > 0 && drawingContent) {
            final int baseAlpha = (mScrimColor & 0xff000000) >>> 24;
            final int imag = (int) (baseAlpha * mScrimOpacity);
            final int color = imag << 24 | (mScrimColor & 0xffffff);
            mScrimPaint.setColor(color);

            canvas.drawRect(clipLeft, 0, clipRight, getHeight(), mScrimPaint);
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mDrawStatusBarBackground && mStatusBarBackground != null) {
            final int inset;
            if (Build.VERSION.SDK_INT >= 21) {
                inset = mLastInsets != null ? mLastInsets.getSystemWindowInsetTop() : 0;
            } else {
                inset = 0;
            }
            if (inset > 0) {
                mStatusBarBackground.setBounds(0, 0, getWidth(), inset);
                mStatusBarBackground.draw(canvas);
            }
        }
    }

    @NonNull
    @Override
    protected Parcelable onSaveInstanceState() {
        Bundle saved = new Bundle();
        saved.putParcelable(KEY_SUPER, super.onSaveInstanceState());

        if (isDrawerOpen(Gravity.LEFT)) {
            saved.putInt(KEY_OPENED_DRAWER, Gravity.LEFT);
        } else if (isDrawerOpen(Gravity.RIGHT)) {
            saved.putInt(KEY_OPENED_DRAWER, Gravity.RIGHT);
        }
        saved.putInt(KEY_LEFT_LOCK_MODER, mLeftLockMode);
        saved.putInt(KEY_RIGHT_LOCK_MODER, mRightLockMode);
        return saved;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Bundle saved = (Bundle) state;
        setDrawerLockMode(saved.getInt(KEY_LEFT_LOCK_MODER, LOCK_MODE_UNLOCKED), Gravity.LEFT);
        setDrawerLockMode(saved.getInt(KEY_RIGHT_LOCK_MODER, LOCK_MODE_UNLOCKED), Gravity.RIGHT);
        openDrawer(saved.getInt(KEY_OPENED_DRAWER, Gravity.NO_GRAVITY));
        super.onRestoreInstanceState(saved.getParcelable(KEY_SUPER));
    }

    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams
                ? new LayoutParams((LayoutParams) p)
                : p instanceof MarginLayoutParams
                ? new LayoutParams((MarginLayoutParams) p)
                : new LayoutParams(p);
    }

    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams && super.checkLayoutParams(p);
    }

    @Override
    public ViewGroup.LayoutParams generateLayoutParams(@NonNull AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }

    public static class LayoutParams extends MarginLayoutParams {

        public int gravity = Gravity.NO_GRAVITY;

        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);

            final TypedArray a = c.obtainStyledAttributes(attrs, LAYOUT_ATTRS);
            this.gravity = a.getInt(0, Gravity.NO_GRAVITY);
            a.recycle();
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(int width, int height, int gravity) {
            this(width, height);
            this.gravity = gravity;
        }

        public LayoutParams(LayoutParams source) {
            super(source);
            this.gravity = source.gravity;
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }

        public LayoutParams(MarginLayoutParams source) {
            super(source);
        }
    }

    private class DragHelperCallback extends ViewDragHelper.Callback {

        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            return true;
        }

        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {
            return child.getTop();
        }

        @Override
        public int clampViewPositionHorizontal(View child, int left, int dx) {

            if (!mIntercepted)
                return child.getLeft();

            if (child == mContentView) {
                if (mLeftState == STATE_CLOSED && mRightState == STATE_CLOSED) {
                    if (dx > 0 && mLeftDrawer != null && mLeftLockMode == LOCK_MODE_UNLOCKED) {
                        slideLeftDrawer(dx);
                        updateDrawerState(mLeftDrawer, STATE_SLIDING);
                    } else if (dx < 0 && mRightDrawer != null && mRightLockMode == LOCK_MODE_UNLOCKED) {
                        slideRightDrawer(dx);
                        updateDrawerState(mRightDrawer, STATE_SLIDING);
                    }
                } else if (mLeftState != STATE_CLOSED) {
                    slideLeftDrawer(dx);
                    updateDrawerState(mLeftDrawer, STATE_SLIDING);
                } else if (mRightState != STATE_CLOSED) {
                    slideRightDrawer(dx);
                    updateDrawerState(mRightDrawer, STATE_SLIDING);
                }
                return child.getLeft();

            } else if (child == mLeftDrawer) {
                updateDrawerState(mLeftDrawer, STATE_SLIDING);
                return child.getLeft() + getActualDxLeft(dx);

            } else if (child == mRightDrawer) {
                updateDrawerState(mRightDrawer, STATE_SLIDING);
                return child.getLeft() + getActualDxRight(dx);

            } else {
                return child.getLeft();
            }
        }

        @Override
        public int getViewHorizontalDragRange(View child) {
            return Integer.MAX_VALUE;
        }
    }

    private static int clamp(int x, int min, int max) {
        if (x > max) return max;
        if (x < min) return min;
        return x;
    }

    private static int lerp(int start, int stop, float amount) {
        return start + (int) ((stop - start) * amount);
    }

    public static boolean isViewUnder(@Nullable View view, int x, int y) {
        if (view == null) {
            return false;
        } else {
            float translationX = 0.0f;
            float translationY = 0.0f;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                translationX = view.getTranslationX();
                translationY = view.getTranslationY();
            }
            return x >= view.getLeft() + translationX &&
                    x < view.getRight() + translationX &&
                    y >= view.getTop() + translationY &&
                    y < view.getBottom() + translationY;
        }
    }

    public interface GestureBlocker {

        /**
         * Returns {@code true} if the gesture should be blocked.
         * It is only checked on the start of the gesture.
         */
        boolean shouldBlockGesture(MotionEvent ev);
    }
}
